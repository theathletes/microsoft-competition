import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import static java.util.regex.Pattern.*;
import static java.util.regex.Matcher.*;
import static java.math.BigInteger.*;
import static java.lang.System.*;
import static java.lang.Integer.*;
import static java.lang.Double.*;
import static java.util.Collections.*;
import static java.lang.Math.*;
import static java.util.Arrays.*;

public class IP
{
	public void run () throws Exception
	{
	
		Scanner cin = new Scanner(new File("JudgeInput.txt"));


    loop: while(cin.hasNextLine())
		{
            String start = cin.next();
            String end = cin.next();
            String test = cin.next();
            int[] startIP = parts(start);
            int[] endIP = parts(end);
            int[] testIP = parts(test);
            if(startIP == null || endIP == null || testIP == null) {
                out.println("InValid");
                continue loop;
            }
            
            /*for(int i = 0; i < 4; i++) {
                if(testIP[i] < startIP[i] || testIP[i] > endIP[i]) {
                    out.println("OutRange");
                    continue loop;
                }
            }
             */
            //out.printf("Low: %d Test: %d High: %d\n", value(startIP), value(testIP), value(endIP));
            if(value(testIP) >= value(startIP) && value(testIP) <= value(endIP)) {
            out.println("InRange");
            } else {
                out.println("OutRange");
            }
            
			
			
			
		}
		

	}
    
    int value(int[] ip) {
        int sum = 0;
        for(int i = 0; i<ip.length; i++) {
            sum = sum << 8;
            sum += ip[i];
        }
        return sum;
    }
    int[] parts(String ip) {
        String[] data = ip.split("\\.");
        if(data.length != 4) {
            return null;
        }
        int[] output = new int[4];
        for(int i = 0; i< data.length; i++) {
           
            output[i] = parseInt(data[i]);
            //out.println(output[i]);
            if(output[i] < 0 || output[i] > 255) {
                return null;
            }
        }
        return output;
    }
	public static void main(String [] args) throws Exception 
	{
		IP a = new IP();
		a.run();
	}
}